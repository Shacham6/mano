﻿using System.Net;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace Mano
{
    public class ManoGame : Game
    {
        private GraphicsDeviceManager _graphicsDeviceManager;
        private SoundEffect _toiletEffect;

        public ManoGame()
        {
            _graphicsDeviceManager = new GraphicsDeviceManager(this);
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            _toiletEffect = Content.Load<SoundEffect>("toilet_flush");
        }
    }
}